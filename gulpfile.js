
// Abaixo com a versão Gulp 4

var gulp = require('gulp');
var sass = require('gulp-sass');
browserSync = require('browser-sync');
imagemin = require('gulp-imagemin');
concat = require('gulp-concat');
del = require('del'),
uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
rev = require('gulp-rev');
cleanCss = require('gulp-clean-css'); 
flatmap = require('gulp-flatmap');
htmlmin = require('gulp-htmlmin');

//compile scss into css
function style(){

//1.where is my scss file
    return gulp.src('./css/*.scss')
//2. pass that file through sass compiler
.pipe(sass())
//3.where do I save the compiled CSS
.pipe(gulp.dest('./css'))
//4.stream changes to all browser
.pipe(browserSync.stream());

}


function watch() {
    browserSync.init({
        server: {
            baseDir: './'
        }
    });
    gulp.watch('./css/*.scss', style);
    gulp.watch('./*.html').on('change', browserSync.reload);
    gulp.watch('/js/*/.js').on('change', browserSync.reload);
}

exports.style = style;
exports.watch = watch;


//5.gulp-imagemin
gulp.task('imagemin', function() {
return gulp.src('dist/images/*')
.pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
.pipe(gulp.dest('dist/'));
});

//6 Gulp-concat uglify compress .js
gulp.task('scripts', function() {
    return gulp.src('dist/js/scripts.js')
    .pipe(concat('scripts'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js'));
});

//7.gulp-clean-css
gulp.task('cleanCss', function() {
    return gulp.src('dist/css/*.css')
    .pipe(cleanCss())
    .pipe(gulp.dest('dist/'));
});

//8.Html
gulp.task('minify', () => {
  return gulp.src('dist/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist/'));
});

//9.Fonts
gulp.task('copyfonts', function() {
    return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot, otf}*')
    .pipe(gulp.dest('./dist/fonts'));
});



// comando para tareas optimizer
gulp.task('build', gulp.series(['cleanCss','imagemin','scripts','minify', 'copyfonts']));
